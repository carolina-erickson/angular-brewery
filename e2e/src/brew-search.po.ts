import { browser, by, element } from 'protractor';

export class BrewSearchPage {


  navigateTo() {
    return browser.get(browser.baseUrl);
  }
  /**
   * Getters
   */
  getTitle() {
    return element(by.css('mat-toolbar span')).getText();
  }

  getNameField () {
    return element(by.name('name'));

  }
  getMinAbvField () {
    return element(by.name('minAbv'));

  }
  getMaxAbvField () {
    return  element(by.name('maxAbv'));
      
  }
  async getTypeField() {
    return await element(by.name('type'));
  }



  getSearchButton() {
    return element(by.name('submit'));
  }

  getCurrentSearchResults() {
    return element(by.css('.results'));
  }

  getNextPage() {}
  getPreviousPage() {}
  getLastPage() {}


  /**
   * Setters
   */
  setNameField () {}
  setMinAbvField () {}
  setMaxAbvField () {}
  setTypeField () {}

  /**
   * Actions
   */
  pressSearch() {}

  selectNextPage() {}


}
