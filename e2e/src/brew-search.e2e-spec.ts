import { BrewSearchPage } from './brew-search.po';
import { by } from '../../node_modules/protractor';

describe('workspace-project App', () => {
  let page: BrewSearchPage;

  beforeEach(() => {
    page = new BrewSearchPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getTitle()).toEqual('Brew Search');
  });

  it('should be able to search by beer name', () => {
    page.navigateTo();
    const nameField = page.getNameField();
    nameField.sendKeys('Ale');
    page.getSearchButton().click();
    expect(page.getCurrentSearchResults().all(by.className('mat-card'))).toBeTruthy();
  });
});