# Angular Brewery!

Run `npm install` on the branch to install dependencies, then `ng serve` to run the project.
Visit `localhost:4200` to access the search app. 

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.0.8.

## Development server

Run `ng serve` from the root directory. Best used on Chrome.

Used [Angular Material](https://material.angular.io) to handle styling. I used a pagination library [ngx-pagination](https://www.npmjs.com/package/ngx-pagination) that worked the best alongside Angular Material to decrease rendering time. 
