import { Component } from '@angular/core';
import { Beer } from '../../models/beer';
import { FormControl, FormGroup, FormBuilder } from '@angular/forms';
import { BrewSearchService } from '../../services/brew-search/brew-search.service';
@Component({
  selector: 'app-brew-search',
  templateUrl: './brew-search.component.html',
  styleUrls: ['./brew-search.component.css']
})
export class BrewSearchComponent {

  beers: Beer[];
  searchForm: FormGroup;
  beerName: FormControl;
  maxRange: FormControl;
  minRange: FormControl;
  beerType: FormControl;

  beerCategories: string[];

  constructor (private brewSearchService: BrewSearchService, private formBuilder: FormBuilder) {
    this.beerName = new FormControl('beerName');
    this.minRange = new FormControl('maxRange');
    this.maxRange = new FormControl('maxRange');
    this.beerType = new FormControl('beerType');
    this.searchForm = this.formBuilder.group({
      beerName: [''],
      minRange: [''],
      maxRange: [''],
      beerType: []
    });
    const typesOfBeers: Beer[] = this.getAllBeers();
    this.beerCategories = this.getBeerCategories(typesOfBeers);
  }

  /**
   * Return all objects from Beer data
   * @returns Beer[]
   */
  getAllBeers (): Beer[] {
    return this.brewSearchService.getAllBeerResults();
  }

  /**
   * Obtain searchForm values and sets a new beers array with the search result
   */
  submit (): void {
    const max: string = this.searchForm.get('maxRange').value;
    const min: string = this.searchForm.get('minRange').value;
    const name: string = this.searchForm.get('beerName').value;
    const type: string = this.searchForm.get('beerType').value;
    const maxNumber: number = max ? parseFloat(max) : 30; // Assuming this is the max abv for any beer
    const minNumber: number = min ? parseFloat(min) : 0;

    if (!name && !min && !max && !type) {
      this.beers = this.getAllBeers();
    } else {
      const beerHolder: Beer[] = this.brewSearchService.getAllBeerResults();
      this.beers = this.brewSearchService.filterBy(beerHolder, name, [minNumber, maxNumber], type);
    }
  }

/**
 * Gets beer categories from cat_name field
 * @param beers
 * @returns string[]
 */
  getBeerCategories (beers: Beer[]): string[] {
    const beerCategories: string[] = beers.map(beer => beer.cat_name);
    return Array.from(new Set(beerCategories)).filter(name => name);
  }
}
