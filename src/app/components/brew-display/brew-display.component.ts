import { Component, Input } from '@angular/core';
import { Beer } from '../../models/beer';

@Component({
  selector: 'app-brew-display',
  templateUrl: './brew-display.component.html',
  styleUrls: ['./brew-display.component.css']
})
export class BrewDisplayComponent {
  p: number = 1;
  @Input() beers: Beer[];

}
