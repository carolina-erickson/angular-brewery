import { Injectable } from '@angular/core';
import { Beer } from '../../models/beer';
import { BEERS } from '../beers';

@Injectable({
  providedIn: 'root'
})
export class BeerService {

/**
 * Grabs all Beer types from data
 */
  public getBeers(): Beer[] {
    return BEERS;
  }
}
