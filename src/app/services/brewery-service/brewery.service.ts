import { Injectable } from '@angular/core';
import { Brewery } from '../../models/brewery';
import { BREWERIES } from '../breweries';

@Injectable({
  providedIn: 'root'
})
export class BreweryService {

  /**
   * Returns a Brewery type by id field
   * @param id
   */
  public getBreweryById (id: string): Brewery {
    return BREWERIES.find(brewery => id === brewery.id);
  }

}
