import { TestBed, inject } from '@angular/core/testing';

import { BrewSearchService } from './brew-search.service';

describe('BrewSearchService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BrewSearchService]
    });
  });

  it('should be created', inject([BrewSearchService], (service: BrewSearchService) => {
    expect(service).toBeTruthy();
  }));
});
