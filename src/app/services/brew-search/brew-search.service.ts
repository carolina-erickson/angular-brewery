import { Injectable } from '@angular/core';
import { BreweryService } from '../brewery-service/brewery.service';
import { BeerService } from '../beer-service/beer.service';
import { Beer } from '../../models/beer';

@Injectable({
  providedIn: 'root'
})
export class BrewSearchService {
  beerService: BeerService;
  breweryService: BreweryService;

  constructor (beerService: BeerService, breweryService: BreweryService) {
    this.beerService = beerService;
    this.breweryService = breweryService;
  }

  /**
   * Completes each Beer object with brewery_name field and returns all of them
   * @returns Beer[]
   */
  getAllBeerResults (): Beer[] {
    const beersResult = this.beerService.getBeers();
    return beersResult.map(item => {
      const breweryResult = this.breweryService.getBreweryById(item.brewery_id);
      if (breweryResult) {
        return {
          ...item,
          brewery_name: breweryResult['name']
        };
      }
      return item;
    });
  }

  /**
   * Returns an array of Beer types as long as all matching conditions are true
   * @param array
   * @param beerName
   * @param abvRange
   * @param beerType
   * @returns Beer[]
   */
  filterBy (array, beerName, abvRange, beerType): Beer[] {
    return array.filter(item => {
      return this.stringMatch(item['name'], beerName) &&
             item['abv'] >= abvRange[0] &&
             item['abv'] <= abvRange[1] &&
             this.stringMatch(item['cat_name'], beerType);
    });
  }

  /**
   * Compares lowercased strings
   * @param item
   * @param name
   * @returns boolean
   */
  stringMatch (item: string, name: string): boolean {
    if (item) {
      const lowerCaseName: string = name ? name.toLowerCase() : '';
      return item.toLowerCase().includes(lowerCaseName);
    }
    return false;
  }
}
