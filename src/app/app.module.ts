import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule, NoopAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';

import { AppComponent } from './app.component';
import { BrewSearchComponent } from './components/brew-search/brew-search.component';
import { BrewDisplayComponent } from './components/brew-display/brew-display.component';
import { MatButtonModule,
        MatFormFieldModule, 
        MatIconModule,
        MatCardModule,
        MatInputModule,
        MatSelectModule,
        MatToolbarModule } from '@angular/material';

@NgModule({
  declarations: [
    AppComponent,
    BrewSearchComponent,
    BrewDisplayComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    NgxPaginationModule,
    MatButtonModule, 
    MatToolbarModule, 
    MatFormFieldModule, 
    MatIconModule,
    MatCardModule, 
    MatInputModule, 
    MatSelectModule,
    BrowserAnimationsModule, 
    NoopAnimationsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
